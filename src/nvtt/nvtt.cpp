// Copyright (c) 2009-2011 Ignacio Castano <castano@gmail.com>
// Copyright (c) 2007-2009 NVIDIA Corporation -- Ignacio Castano <icastano@nvidia.com>
// 
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

#include "nvtt.h"
#include "nvcore/nvcore.h"

#include "nvcore/MemStream.h"
#include "nvimage/DirectDrawSurface.h"
#include "nvimage/Image.h"
#include "nvmath/Color.h"

#include <vector>

using namespace nvtt;

// Return a string for the given error.
const char * nvtt::errorString(Error e)
{
    NV_COMPILER_CHECK(Error_Count == 7);
    switch(e)
    {
        case Error_Unknown:
            return "Unknown error";
        case Error_InvalidInput:
            return "Invalid input";
        case Error_UnsupportedFeature:
            return "Unsupported feature";
        case Error_CudaError:
            return "CUDA error";
        case Error_FileOpen:
            return "Error opening file";
        case Error_FileWrite:
            return "Error writing through output handler";
        case Error_UnsupportedOutputFormat:
            return "The container file does not support the selected output format";
    }

    return "Invalid error";
}

// Return NVTT version.
unsigned int nvtt::version()
{
    return NVTT_VERSION;
}


struct MyOutputHandler : public nvtt::OutputHandler
{
	MyOutputHandler(UpdateProgress updateProgressPtr) : updateProgressPtr(updateProgressPtr), total(0), progress(0), percentage(0)
	{
		stream = new nv::MemoryOutputStream();
	}

	void setTotal(int64 t)
	{
		total = t + 128;
	}
	void setDisplayProgress(bool b)
	{
		verbose = b;
	}

	virtual ~MyOutputHandler()
	{
		delete stream;
	}

	virtual void beginImage(int size, int width, int height, int depth, int face, int miplevel)
	{
		// ignore.
	}

	// Output data.
	virtual bool writeData(const void * data, int size)
	{
		nvDebugCheck(stream != NULL);
		stream->serialize(const_cast<void *>(data), size);

		progress += size;
		int p = int((100 * progress) / total);
		if (verbose && p != percentage)
		{
			nvCheck(p >= 0);

			percentage = p;
			updateProgressPtr(percentage);

			//printf("\rCompressing: %d%%", percentage);
			//fflush(stdout);
		}

		return true;
	}

	virtual void endImage()
	{
		// ignore.
	}

	int64 total;
	int64 progress;
	int percentage;
	bool verbose;
	nv::MemoryOutputStream * stream;

	UpdateProgress updateProgressPtr;
};

struct MyErrorHandler : public nvtt::ErrorHandler
{
	virtual void error(nvtt::Error e)
	{
#if _DEBUG
		nvDebugBreak();
#endif
		printf("Error: '%s'\n", nvtt::errorString(e));
	}
};

// Set color to normal map conversion options.
/*void setColorToNormalMap2(nvtt::InputOptions & inputOptions)
{
	inputOptions.setNormalMap(false);
	inputOptions.setConvertToNormalMap(true);
	inputOptions.setHeightEvaluation(1.0f / 3.0f, 1.0f / 3.0f, 1.0f / 3.0f, 0.0f);
	//inputOptions.setNormalFilter(1.0f, 0, 0, 0);
	//inputOptions.setNormalFilter(0.0f, 0, 0, 1);
	inputOptions.setGamma(1.0f, 1.0f);
	inputOptions.setNormalizeMipmaps(true);
}

// Set options for normal maps.
void setNormalMap2(nvtt::InputOptions & inputOptions)
{
	inputOptions.setNormalMap(true);
	inputOptions.setConvertToNormalMap(false);
	inputOptions.setGamma(1.0f, 1.0f);
	inputOptions.setNormalizeMipmaps(true);
}*/

// Set options for color maps.
void setColorMap2(nvtt::InputOptions & inputOptions)
{
	inputOptions.setNormalMap(false);
	inputOptions.setConvertToNormalMap(false);
	inputOptions.setGamma(2.2f, 2.2f);
	inputOptions.setNormalizeMipmaps(false);
}

uint8_t* nvtt::CompressTexture(uint8_t* input, int width, int height, int* outputSize, DXTCompression compression, UpdateProgress updateProgressPtr)
{
	nvtt::InputOptions inputOptions;
	inputOptions.setTextureLayout(nvtt::TextureType_2D, width, height);
	inputOptions.setMipmapData(input, width, height);
	inputOptions.setWrapMode(nvtt::WrapMode_Clamp);
	inputOptions.setAlphaMode(nvtt::AlphaMode_Transparency);
	//inputOptions.setAlphaMode(nvtt::AlphaMode_None);
	setColorMap2(inputOptions);

	nvtt::CompressionOptions compressionOptions;

	nvtt::Format format;

	if (compression == nv::DXTCompression::DXT3)
		format = nvtt::Format_BC2;
	else if (compression == nv::DXTCompression::DXT5)
		format = nvtt::Format_BC3;
	else
		format = nvtt::Format_BC1a;

	compressionOptions.setFormat(format);
	compressionOptions.setQuality(nvtt::Quality_Normal);

	MyOutputHandler outputHandler(updateProgressPtr);
	if (outputHandler.stream->isError())
	{
		return NULL;
	}

	nvtt::Compressor compressor;

	outputHandler.setTotal(compressor.estimateSize(inputOptions, compressionOptions));
	outputHandler.setDisplayProgress(true);

	nvtt::OutputOptions outputOptions;
	outputOptions.setOutputHandler(&outputHandler);
	outputOptions.setOutputHeader(false);

	if (!compressor.process(inputOptions, compressionOptions, outputOptions))
	{
		return NULL;
	}

	*outputSize = outputHandler.stream->size();

	uint8_t* result = (uint8_t*)malloc(*outputSize);
	memcpy_s(result, *outputSize, &outputHandler.stream->getData()[0], *outputSize);

	return result;
}

bool nvtt::DecompressTexture(uint8_t* input, int inputSize, uint8_t* output, int width, int height, DXTCompression compression)
{
	nv::DirectDrawSurface dds(input, inputSize, width, height, (nv::DXTCompression)compression);
	if (!dds.isValid())
		return false;

	nv::Image img;
	dds.mipmap(&img, 0, 0);

	nv::Color32* pixels = img.pixels();

	int index = 0;
	for (int i = 0; i < width * height; i++)
	{
		nv::Color32 color = pixels[i];
		output[index + 0] = color.a;
		output[index + 1] = color.r;
		output[index + 2] = color.g;
		output[index + 3] = color.b;
		index += 4;
	}

	return true;
}

