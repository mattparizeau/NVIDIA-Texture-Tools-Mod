#ifndef _MEMSTREAM_H_
#define _MEMSTREAM_H_

#include <nvcore\Stream.h>
#include <nvcore\StdStream.h>

#include <vector>

namespace nv
{
	/// Memory input stream.
	class NVCORE_CLASS MemoryOutputStream : public Stream
	{
		NV_FORBID_COPY(MemoryOutputStream);
	public:

		/// Ctor.
		MemoryOutputStream() : m_pos(0)
		{

		}

		/** @name Stream implementation. */
		//@{
		/// Read data.
		virtual uint serialize(void * data, uint len)
		{
			nvDebugCheck(data != NULL);
			nvDebugCheck(!isError());

			uint8* d = (uint8*)data;

			for (int i = 0; i < len; i++)
			{
				////////this->data.push_back(d[i]);

				int index = m_pos + i;

				if (index >= this->data.size())
				{
					this->data.push_back(0x0);
				}
				this->data[index] = d[i];
			}

			m_pos += len;

			/*uint left = m_size - tell();
			if (len > left) len = left;

			memcpy(data, m_ptr, len);
			m_ptr += len;*/

			return len;
		}

		virtual bool isLoading() const
		{
			return false;
		}

		virtual bool isSaving() const
		{
			return true;
		}
		//@}

		/** @name Stream implementation. */
		//@{
		virtual void seek(uint pos)
		{
			m_pos = pos;
		}

		virtual uint tell() const
		{
			return m_pos;
		}

		virtual uint size() const
		{
			return data.size();
		}

		virtual bool isError() const
		{
			return false;
		}

		virtual void clearError()
		{
			return;
		}

		virtual bool isAtEnd() const
		{
			return false;
		}

		/// Always true.
		virtual bool isSeekable() const { return true; }

		std::vector<uint8> getData()
		{
			return data;
		}

	private:

		uint m_pos;
		std::vector<uint8> data;

	};
}

#endif // _MEMSTREAM_H_
